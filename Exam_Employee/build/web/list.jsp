<%-- 
    Document   : list
    Created on : Jan 8, 2020, 9:21:22 AM
    Author     : HP
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>List Page</title>
    </head>
    <body>
    <center>
        <h2>Employee List</h2>
        <table style="border: 1px solid black">
            <th style="border: 1px solid black">ID</th>
            <th style="border: 1px solid black">Full name</th>
            <th style="border: 1px solid black">Birthday</th>
            <th style="border: 1px solid black">Address</th>
            <th style="border: 1px solid black">Position</th>
            <th style="border: 1px solid black">Department</th>
                <c:forEach var="emp" items="${getAllEmployee}">
                <tr style="border: 1px solid black">
                    <td style="border: 1px solid black">${emp.id}</td>
                    <td style="border: 1px solid black">${emp.fullname}</td>
                    <td style="border: 1px solid black">${emp.birthday}</td>
                    <td style="border: 1px solid black">${emp.address}</td>
                    <td style="border: 1px solid black">${emp.position}</td>
                    <td style="border: 1px solid black">${emp.department}</td>
                </tr>
            </c:forEach>
        </table>
    </center>
</body>
</html>
